console.log("Inisiasi aja");

/* 
* 'Limas', ari
* 'Prisma', tantri
* 'Tabung', piter
* 'Bola', dyaz
*/


function volPrisma(alas, tinggi, tinggiPrisma) {
    const volumePrisma = (1/2 * alas * tinggi) * tinggiPrisma;
    return volumePrisma;
}
console.log(volPrisma(3,4,4));

const PHI = 3.14;

function volBola(r) {
  return hasil = 4 / 3 * PHI * r ** 3;
}

console.log(volBola(6));

function volumeTabung(r, t) {
  let hasil
  if (r % 7 == 0 || t % 7 == 0) {
    hasil = 22 / 7 * Math.pow(r, 2) * t
    return hasil;
  } else {
    hasil = 3.14 * Math.pow(r, 2) * t
    return hasil;
  }
}


// Menghitung volume limas
function volumeLimas(alas, tinggi, tinggiLimas) {
    const volumeAlas = (alas * alas * tinggi) / 3;
    const volumeTutup = (alas * tinggiLimas) / 3;
    return volumeAlas + volumeTutup;
  }
  
  // Menghitung luas permukaan limas
  function luasPermukaanLimas(alas, tinggi, tinggiLimas) {
    const sisiMiring = Math.sqrt(Math.pow(alas / 2, 2) + Math.pow(tinggiLimas, 2));
    const luasAlas = alas * alas;
    const luasSegitiga = alas * sisiMiring / 2;
    const luasSelimut = (alas + sisiMiring) * tinggi / 2;
    return luasAlas + 2 * luasSegitiga + luasSelimut;
  }
  
  const alas = 8;
const tinggi = 12;
const tinggiLimas = 10;

const volume = volumeLimas(alas, tinggi, tinggiLimas);
const luasPermukaan = luasPermukaanLimas(alas, tinggi, tinggiLimas);

console.log('Volume limas: ' + volume);
console.log('Luas permukaan limas: ' + luasPermukaan);
